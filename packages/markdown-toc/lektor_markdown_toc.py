# -*- coding: utf-8 -*-
from lektor.pluginsystem import Plugin
from lektor.utils import slugify
from markupsafe import Markup
from collections import namedtuple


class MarkdownTocPlugin(Plugin):
    name = u'Markdown TOC'
    description = u'Add section numbers, anchors and a TOC to your Markdown.'

    def increment_section(self, level):
        # Properly handle documents that start from an h2.
        while len(self._section_number) < level:
            self._section_number.append(0)

        # Increment section number
        self._section_number[level-1] += 1

        # Cut off any extras.
        self._section_number = self._section_number[:level]

    @property
    def section(self):
        """Return the current section as a string."""
        # Remove any leading zeroes.
        trimmed = []
        for number in self._section_number:
            # If there are any items in the list already...
            if trimmed:
                # Change all zeroes to ones.
                trimmed.append(number if number else 1)
            elif number:
                # Otherwise skip leading zeroes.
                trimmed.append(number)

        return ".".join(str(x) for x in trimmed)

    def on_markdown_meta_init(self, *args, **kwargs):
        # Reset the things
        self._enable_numbering = False
        self._section_number = []

    def on_markdown_config(self, config, **extra):
        class HeaderAnchorMixin(object):
            def header(renderer, text, level, raw):
                # Magic incantation to enable numbering.
                if text == "enable_section_numbering":
                    self._enable_numbering = True
                    return ""

                if not self._enable_numbering:
                    return '<h%d>%s</h%d>' % (level, text, level)

                self.increment_section(level)
                anchor = slugify(raw)
                return '<h%d id="%s">%s. %s<a class="anchor" href="#%s-%d" title="Link to this section"> &#182;</a></h%d>' % (level, anchor, self.section, text, anchor, level, level)
        config.renderer_mixins.append(HeaderAnchorMixin)
